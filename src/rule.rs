//! Rule declarations to abstract over different variants of chess.

use std::str::FromStr;

use crate::{bitboard, plygen};
use crate::board::{
  Bitboard, CastleSide, Centipawn, Color::{self, *}, Piece::{self, *},
  Ply::{self, *}, Rank, Square::{self, *}
};
use crate::format::Fen;
use crate::position::{Ending, Position};

use enum_map::EnumMap;


/// Should contain no state associated with a particular situation, and should
/// only has immutable receivers to extract information about a general
/// situation.
pub trait Ruleset {
  type Position;
  // type Pieceset: Enum;
  // maybe Plyset can implement traits for Moving, Promoting, Castling, etc...
  // type Plyset;
  // type Winset;

  fn start(&self) -> Self::Position;

  fn state(&self, pos: &Self::Position) -> Result<Color, Ending>;

  /// A bitboard of squares that are attacking this square.
  /// The popcount of one of these squares is the number of pieces attacking it.
  fn attack_map(&self, pos: &Self::Position) -> EnumMap<Square, Bitboard>;
  /// A bitboard of squares that are being attacked by this square.
  /// The popcount of one of these squares is the number of pieces being attacked.
  fn defend_map(&self, pos: &Self::Position) -> EnumMap<Square, Bitboard>;
}


pub trait Material: Ruleset {
  fn material(&self, pos: &Self::Position) -> EnumMap<Color, Centipawn>;
}


pub trait PlayPly {
  type Position;
  type Plytype;
  type Err;

  fn ply(&self, pos: &mut Self::Position, ply: Self::Plytype) -> Result<(), Self::Err>;
}


pub trait UndoPly: PlayPly {
  fn undo_ply(&self, pos: &mut Self::Position, ply: Self::Plytype) -> Result<(), Self::Err>;
}


#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Vanilla;

impl Ruleset for Vanilla {
  type Position = Position;

  fn start(&self) -> Self::Position {
    let fen = Fen::from_str("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1").unwrap();
    Position {
      board: fen.board.into(),
      turn: fen.turn,
      castling: fen.castling,
      en_passant_target: fen.en_passant_target,
      plys_since_progress: fen.plys_since_progress,
    }
  }

  fn state(&self, pos: &Self::Position) -> Result<Color, Ending> { todo!() }

  /// A mapping representing for each square on the board, what is the bitboard
  /// of all squares being attacked by this square.
  fn attack_map(&self, pos: &Self::Position) -> EnumMap<Square, Bitboard> {
    pos.board.squares()
      .map(|(piece, square)| {
        let board = plygen::pseudo_plys(piece, square, pos);
          // todo: king pins

        (square, board)
      })
      .fold(<EnumMap<Square, Bitboard> as Default>::default(), |mut acc, (square, board)| {
        acc[square] = board;
        acc
      })

    // unoccupied squares cannot attack anything
    // for square in occupied.squares().map(|square| (square, pos.board.)) {
    //   base_moves(color, piece, square)
    // }

  }

  fn defend_map(&self, pos: &Self::Position) -> EnumMap<Square, Bitboard> { todo!() }
}

impl Material for Vanilla {
  fn material(&self, pos: &Self::Position) -> EnumMap<Color, Centipawn> {
    // todo: simd?

    let map: EnumMap<Piece, Centipawn> = enum_map! {
      Pawn => 1.into(),
      Bishop => 3.5.into(),
      Knight => 3.5.into(),
      Rook => 5.25.into(),
      Queen => 10.into(),
      King => i16::MAX.into(),
    };

    pos.board.pieces().into_iter()
      .map(|(color, pieces)| {
        let sum = pieces.into_iter()
          .map(|(piece, board)| Centipawn(board.count_ones() as i16) * map[piece])
          .fold(Centipawn(0), |acc, item| acc.saturating_add(item));
        (color, sum)
      })
      .collect::<EnumMap<_, _>>()
  }
}

impl PlayPly for Vanilla {
  type Position = Position;
  type Plytype = Ply;
  type Err = ();  // todo: give reason for invalid move?

  // todo: maybe assume move is valid?
  fn ply(&self, pos: &mut Self::Position, ply: Ply) -> Result<(), Self::Err> { todo!() }
}

impl UndoPly for Vanilla {
  fn undo_ply(&self, pos: &mut Self::Position, ply: Self::Plytype) -> Result<(), Self::Err> {
    todo!()
  }
}


#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct HillKing;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct TripleCheck;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Antichess;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Atomic;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Horde;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct RacingKings;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Crazyhouse;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Chess960;


#[cfg(test)]
mod test {
  use crate::{bitboard, board::*, position::*};

  // #[test]
  // fn pawn_move() {
  //   let pos = Position {
  //     board, turn: White, castling: CastleState::BOTH, en_passant_target: None, plys_since_progress: 0
  //   };
  //   assert_eq!(, );
  // }
}
