//! A representation of a specific state of the game.

use crate::board::{Board, CastleState, Color, Ply, Piece, Square};

use enum_map::EnumMap;

// todo: consider reimplementing accessor traits for features such as castling, en passant, etc.

// todo: this does not handle threefold repetition, and that's probably fine for now

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Position {
  pub board: Board,
  pub turn: Color,
  pub castling: EnumMap<Color, CastleState>,
  /// The current square eligible for pawn capture via en-passant, if available.
  /// Set on the third or sixth rank when any player plays a double pawn move.
  pub en_passant_target: Option<Square>,
  /// 100-ply (50-move) rule counter; reset to zero every time a capture or pawn
  /// move is played.
  pub plys_since_progress: u16,
}

// impl TryFrom<Fen> for Position {
//   type Error;

//   fn try_from(value: Fen) -> Result<Self, Self::Error> { todo!() }
// }


#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct TripleCheckPosition(pub Position, pub u8);

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct CrazyhousePosition {
  pub situ: Position,
  pub hand: EnumMap<Color, EnumMap<Piece, u8>>,
}


#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum Ending {
  Checkmate(Color),
  Stalemate,
}


#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Game {
  current: Position,
  moves: Vec<Ply>,
  /*
   *  * castling: ,
   *  * state: */
}

impl Game {
  pub fn into_pgn(&self) -> String { todo!() }
}
