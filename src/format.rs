//! Definitions for many standard ascii chess formats, including parsers.
//!
//! Structures in this module all contain a [FromStr](std::str::FromStr) and a
//! [Display](std::fmt::Display) implementation, in addition to having a
//! parser implementing [Parse](pcomb::Parse) defined.

use std::{iter, str::FromStr};

use enum_map::{EnumMap, enum_map};
use pcomb::{Parse, Match, parsers::str, ParseError};

use crate::{board::{Color::{self, *}, CastleState, AltBoard, Piece::{self, *}, Square, ColoredPiece, File, Rank}, util::CollectArray};


pub fn square(input: &str) -> Result<(&str, Square), ParseError> {
  let file = str::alpha.map(|alpha| {
    File::try_from(
      alpha.chars().next().expect("alpha guarantees one char exists")
    )
  });

  let rank = str::digit_parse.map(|num| Rank::try_from(num));

  let (rest, res) = file.fuse(rank)
    .map(|(file, rank)| {
      Ok(Square::from_coords(file?, rank?))
    })
    .parse(input)?;

  Ok((rest, res.map_err(|_: ()| ParseError::Reject)?))
}


pub struct PureCoordAlgebraic {
  pub from: Square,
  pub to: Square,
  pub promoted: Option<Piece>,
}

impl FromStr for PureCoordAlgebraic {
  type Err = ParseError;

  fn from_str(input: &str) -> Result<Self, Self::Err> {
    let (_, out) = pure_coord_algebraic.parse(input)?;
    Ok(out)
  }
}


pub fn pure_coord_algebraic(input: &str) -> Result<(&str, PureCoordAlgebraic), ParseError> {
  square.fuse(square)
    .fuse(
      Match("n").map(|_| Knight)
        .or(Match("b").map(|_| Bishop))
        .or(Match("r").map(|_| Rook))
        .or(Match("q").map(|_| Queen))
        .maybe()
    )
    .map(|((from, to), promoted)| PureCoordAlgebraic { from, to, promoted })
    .parse(input)
}


#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Fen {
  pub board: AltBoard,
  pub turn: Color,
  pub castling: EnumMap<Color, CastleState>,
  /// The current square eligible for pawn capture via en-passant, if available.
  /// Set on the third or sixth rank when any player plays a double pawn move.
  pub en_passant_target: Option<Square>,
  /// 100-ply (50-move) rule counter; reset to zero every time a capture or pawn
  /// move is played.
  pub plys_since_progress: u16,
  pub move_clock: u16,
}

impl FromStr for Fen {
  type Err = FenError;

  fn from_str(input: &str) -> Result<Self, Self::Err> {
    let (_, out) = fen.parse(input)?;
    Ok(out)
  }
}


#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum FenError {
  InvalidBoard,
  InvalidTurn,
  InvalidCastling,
  InvalidEnPassant,
  InvalidPlyCapturePawn,
  InvalidMoveClock,
}


pub fn fen(input: &str) -> Result<(&str, Fen), FenError> {
  const PIECES: [Option<ColoredPiece>; 12] = [
    Some(ColoredPiece::new(White, Pawn)),
    Some(ColoredPiece::new(White, Knight)),
    Some(ColoredPiece::new(White, Bishop)),
    Some(ColoredPiece::new(White, Rook)),
    Some(ColoredPiece::new(White, Queen)),
    Some(ColoredPiece::new(White, King)),
    Some(ColoredPiece::new(Black, Pawn)),
    Some(ColoredPiece::new(Black, Knight)),
    Some(ColoredPiece::new(Black, Bishop)),
    Some(ColoredPiece::new(Black, Rook)),
    Some(ColoredPiece::new(Black, Queen)),
    Some(ColoredPiece::new(Black, King)),
  ];

  let fen_rank =
    Match("P").map(|_| &PIECES[0..1])
    .or(Match("N").map(|_| &PIECES[1..2]))
    .or(Match("B").map(|_| &PIECES[2..3]))
    .or(Match("R").map(|_| &PIECES[3..4]))
    .or(Match("Q").map(|_| &PIECES[4..5]))
    .or(Match("K").map(|_| &PIECES[5..6]))
    .or(Match("p").map(|_| &PIECES[6..7]))
    .or(Match("n").map(|_| &PIECES[7..8]))
    .or(Match("b").map(|_| &PIECES[8..9]))
    .or(Match("r").map(|_| &PIECES[9..10]))
    .or(Match("q").map(|_| &PIECES[10..11]))
    .or(Match("k").map(|_| &PIECES[11..12]))
    .or(Match("1").map(|_| &[None; 1][..]))
    .or(Match("2").map(|_| &[None; 2][..]))
    .or(Match("3").map(|_| &[None; 3][..]))
    .or(Match("4").map(|_| &[None; 4][..]))
    .or(Match("5").map(|_| &[None; 5][..]))
    .or(Match("6").map(|_| &[None; 6][..]))
    .or(Match("7").map(|_| &[None; 7][..]))
    .or(Match("8").map(|_| &[None; 8][..]))
    .repeat_greedy(Vec::new())
    .map_err(|_| FenError::InvalidBoard)
    .chain(|files: Vec<&'static [Option<ColoredPiece>]>| -> Result<(Vec<&'static [Option<ColoredPiece>]>, _), FenError> {
      let flattened = files.into_iter().flatten().cloned();
      Ok((Vec::with_capacity(0), flattened.collect_array_exact::<8>().ok_or(FenError::InvalidBoard)?))
    });

  let (rest, board) = fen_rank.clone().fuse(Match("/").map_err(|_| FenError::InvalidBoard))
    .first()
    .repeat_const::<7>()
    .fuse(fen_rank.map_err(|_| FenError::InvalidBoard))
    .fuse(Match(" ").map_err(|_| FenError::InvalidBoard)).first()
    .map(|(arr, eighth)| {
      let out = arr.into_iter()
        .chain(iter::once(eighth))
        .rev()  // FEN's ranks are in reversed order from eighth to first
        .flatten()
        .collect_array::<64>()
        .expect("we verified there are 8 files of 8 pieces each");

      AltBoard::new(out)
    }).parse(input)?;

  let (rest, turn) = Match("w").map(|_| White)
    .or(Match("b").map(|_| Black))
    .fuse(Match(" ")).first()
    .map_err(|_| FenError::InvalidTurn)
    .parse(rest)?;

  let (rest, castling) =
    Match("K").map(|_| CastleState::KingSide).maybe().unwrap_or(CastleState::NONE)
    .fuse(
      Match("Q").map(|_| CastleState::QueenSide).maybe().unwrap_or(CastleState::NONE)
    ).fuse(
      Match("k").map(|_| CastleState::KingSide).maybe().unwrap_or(CastleState::NONE)
    ).fuse(
      Match("q").map(|_| CastleState::QueenSide).maybe().unwrap_or(CastleState::NONE)
    ).fuse(Match(" ")).first()
    .map(|(((white_ks, white_qs), black_ks), black_qs)| {
      enum_map! { White => white_ks | white_qs, Black => black_ks | black_qs }
    })
    .or(Match("-").map(|_| enum_map! { White => CastleState::NONE, Black => CastleState::NONE }))
    .map_err(|_| FenError::InvalidCastling)
    .parse(rest)?;

  let (rest, en_passant_target) = square.map(|squ| Some(squ))
    .or(Match("-").map(|_| None))
    .fuse(Match(" ")).first()
    .map_err(|_| FenError::InvalidEnPassant)
    .parse(rest)?;

  let (rest, plys_since_progress) = str::integer
    .fuse(Match(" ")).first()
    .map_err(|_| FenError::InvalidPlyCapturePawn)
    .parse(rest)?;

  let (rest, move_clock) = str::integer
    .map_err(|_| FenError::InvalidMoveClock)
    .parse(rest)?;

  Ok((
    rest,
    Fen {
      board,
      turn,
      castling,
      en_passant_target,
      plys_since_progress,
      move_clock,
    }
  ))
}


#[cfg(test)]
mod test {
  use crate::{board::Board, bitboard};

  use super::*;

  use pcomb::Parse;

  #[test]
  fn fen_test() {
    assert_eq!(
      fen.parse("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"),
      Ok(("", Fen {
        board: {
          let mut board = Board::default();

          unsafe {
            board.pieces_mut()[White][Pawn] = bitboard!(
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
              11111111
              00000000
            );
            board.pieces_mut()[White][Knight] = bitboard!(
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
              01000010
            );
            board.pieces_mut()[White][Bishop] = bitboard!(
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
              00100100
            );
            board.pieces_mut()[White][Rook] = bitboard!(
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
              10000001
            );
            board.pieces_mut()[White][Queen] = bitboard!(
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
              00010000
            );
            board.pieces_mut()[White][King] = bitboard!(
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
              00001000
            );

            board.pieces_mut()[Black][Pawn] = bitboard!(
              00000000
              11111111
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
            );
            board.pieces_mut()[Black][Knight] = bitboard!(
              01000010
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
            );
            board.pieces_mut()[Black][Bishop] = bitboard!(
              00100100
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
            );
            board.pieces_mut()[Black][Rook] = bitboard!(
              10000001
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
            );
            board.pieces_mut()[Black][Queen] = bitboard!(
              00010000
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
            );
            board.pieces_mut()[Black][King] = bitboard!(
              00001000
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
              00000000
            );
          }

          board.into()
        },
        turn: White,
        castling: enum_map! { White => CastleState::BOTH, Black => CastleState::BOTH },
        en_passant_target: None,
        plys_since_progress: 0,
        move_clock: 1,
      }))
    );
  }
}
