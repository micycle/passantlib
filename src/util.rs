use std::mem::{self, MaybeUninit};


pub(crate) trait CollectArray: Iterator
where Self: Sized {
  fn collect_array<const S: usize>(mut self) -> Option<[Self::Item; S]> {
    // Source derived from "Initializing an array element-by-element" from
    // Rust 1.36 documentation of `std::mem::MaybeUninit`.

    // Create an uninitialized array of MaybeUninit. assume_init is
    // safe because the type we are claiming to have initialized here (ie.
    // [MaybeUninit<Self::Item>; S]) is a bunch of MaybeUninits, which do not
    // require initialization.
    let mut arr: [MaybeUninit<Self::Item>; S] = unsafe {
      MaybeUninit::uninit().assume_init()
    };

    // Dropping a MaybeUninit does nothing. Thus using raw pointer
    // assignment instead of `ptr::write` does not cause the old
    // uninitialized value to be dropped.
    for i in 0..S {
      match self.next() {
        Some(item) => arr[i] = MaybeUninit::new(item),
        None => {
          // If this function needs to return early, then we need to drop the
          // previously initialized elements in order to avoid leaking memory.
          for i in 0..i {
            let item = mem::replace(&mut arr[i], MaybeUninit::uninit());
            mem::drop(unsafe { item.assume_init() });
          }
          return None;
        }
      }
    }

    // All elements are initialized now
    // transmute_copy does not drop arr for us, however arr is an array of
    // MaybeUninit, which has an empty drop implementation anyways.
    Some(unsafe {
      mem::transmute_copy::<[MaybeUninit<Self::Item>; S], [Self::Item; S]>(&arr)
    })
  }

  fn collect_array_exact<const S: usize>(mut self) -> Option<[Self::Item; S]> {
    // Source derived from "Initializing an array element-by-element" from
    // Rust 1.36 documentation of `std::mem::MaybeUninit`.

    // Create an uninitialized array of MaybeUninit. assume_init is
    // safe because the type we are claiming to have initialized here (ie.
    // [MaybeUninit<Self::Item>; S]) is a bunch of MaybeUninits, which do not
    // require initialization.
    let mut arr: [MaybeUninit<Self::Item>; S] = unsafe {
      MaybeUninit::uninit().assume_init()
    };

    // Dropping a MaybeUninit does nothing. Thus using raw pointer
    // assignment instead of `ptr::write` does not cause the old
    // uninitialized value to be dropped.
    for i in 0..S {
      match self.next() {
        Some(item) => arr[i] = MaybeUninit::new(item),
        None => {
          // If this function needs to return early, then we need to drop the
          // previously initialized elements in order to avoid leaking memory.
          for i in 0..i {
            let item = mem::replace(&mut arr[i], MaybeUninit::uninit());
            mem::drop(unsafe { item.assume_init() });
          }
          return None;
        }
      }
    }

    if let Some(_) = self.next() {
      for i in 0..S {
        let item = mem::replace(&mut arr[i], MaybeUninit::uninit());
        mem::drop(unsafe { item.assume_init() });
      }
      return None;
    }

    // All elements are initialized now
    // transmute_copy does not drop arr for us, however arr is an array of
    // MaybeUninit, which has an empty drop implementation anyways.
    Some(unsafe {
      mem::transmute_copy::<[MaybeUninit<Self::Item>; S], [Self::Item; S]>(&arr)
    })
  }
}

impl<I: Iterator> CollectArray for I {}


#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn collect() {
    let arr = (1..=5).map(|i| i * 2).collect_array::<5>();
    assert_eq!(arr, Some([2, 4, 6, 8, 10]));
  }

  #[test]
  fn collect_bad() {
    let arr = (1..=5).map(|i| i * 2).collect_array::<9>();
    assert_eq!(arr, None);
  }
}
