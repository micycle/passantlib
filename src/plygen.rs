//! Ply generation based on a specific position.

use crate::bitboard;
use crate::board::{Bitboard, Color, ColoredPiece, File, Piece::{self, *}, Rank, Square};
use crate::position::Position;


/// Gives the maximal set of all possible moves for which a given piece might be
/// able to perform, given context-free information about the piece and square.
/// This can produce illegal moves in cases where board or previous move context
/// is required, as in:
///
/// - King pins
/// - King in check
/// - Capturing of own pieces
/// - En passant, which is always considered valid in this function
/// - Illegal moves in certain invariants (eg. antichess, racing kings)
///
/// In addition, this fails to compensate for non-standard moves, such as
/// promotion, castling, or moves from variants (eg. crazyhouse)
pub fn base_plys(cp: ColoredPiece, squ: Square) -> Bitboard {
  match cp.get() {
    // this doesn't account for contextual en-passant
    (Color::White, Piece::Pawn) => {
      let double_move = bitboard!(
        00000000
        00000000
        00000000
        00000000
        00000000
        01000000
        00000000
        00000000
      )
      .shift_vert(squ.rank() as i8)
      .shift_horz(squ.file() as i8 - File::B as i8)
        // a double move is valid when the target square is on the 4th rank
        & Bitboard::from_rank(Rank::Fourth);

      bitboard!(
        00000000
        00000000
        00000000
        00000000
        00000000
        00000000
        11100000
        00000000
      )
      .shift_vert(squ.rank() as i8)
      .shift_horz(squ.file() as i8 - File::B as i8)
        | double_move
    },

    (Color::Black, Piece::Pawn) => {
      let double_move = bitboard!(
        00000000
        00000000
        01000000
        00000000
        00000000
        00000000
        00000000
        00000000
      )
      .shift_vert(squ.rank() as i8 - Rank::Eight as i8)
      .shift_horz(squ.file() as i8 - File::B as i8)
        // a double move is valid when the target square is on the 5th rank
        & Bitboard::from_rank(Rank::Fifth);

      bitboard!(
        00000000
        11100000
        00000000
        00000000
        00000000
        00000000
        00000000
        00000000
      )
      .shift_vert(squ.rank() as i8 - Rank::Eight as i8)
      .shift_horz(squ.file() as i8 - File::B as i8)
        | double_move
    },

    (_, Piece::Rook) => {
      (Bitboard::from_file(squ.file()) | Bitboard::from_rank(squ.rank())) & !Bitboard::from(squ)
    },

    (_, Piece::Knight) => bitboard!(
      00000000
      00000000
      00000000
      01010000
      10001000
      00000000
      10001000
      01010000
    )
    .shift_vert(squ.rank() as i8 - Rank::Third as i8)
    .shift_horz(squ.file() as i8 - File::C as i8),

    (_, Piece::Bishop) => {
      bitboard!(
        10000000
        01000000
        00100000
        00010000
        00001000
        00000100
        00000010
        00000001
      )
      .shift_vert(squ.file() as i8 + squ.rank() as i8 - 7)
        | bitboard!(
          00000001
          00000010
          00000100
          00001000
          00010000
          00100000
          01000000
          10000000
        )
        .shift_vert(squ.rank() as i8 - squ.file() as i8)
          & !Bitboard::from(squ)
    },

    (color, Piece::Queen) => {
      base_plys(ColoredPiece::new(color, Piece::Rook), squ) | base_plys(ColoredPiece::new(color, Piece::Bishop), squ)
    },

    (_, Piece::King) => bitboard!(
      00000000
      00000000
      00000000
      00000000
      00000000
      11100000
      10100000
      11100000
    )
    .shift_vert(squ.rank() as i8 - Rank::Second as i8)
    .shift_horz(squ.file() as i8 - File::B as i8),
  }
}

/// Gives the set of pseudo legal moves for which a piece can perform
/// considering the board state.
/// A pseudo legal move is one which is legal, except for the ugly side-case of
/// king pins, or if the king is in check.
pub fn pseudo_plys(cp: ColoredPiece, squ: Square, pos: &Position) -> Bitboard {
  let (color, piece) = cp.get();

  let plys = match piece {
    Pawn => {
      let plys = base_plys(cp, squ);

      match pos.en_passant_target {
        // strip both en passant squares, then or the target square
        Some(target) => (plys & Bitboard::from_file(squ.file())) | Bitboard::from(target),
        // the pawn can only move onto it's own file
        None => plys & Bitboard::from_file(squ.file()),
      }
    },

    Knight => base_plys(cp, squ),  // the knight has no specific movement restrictions

    Bishop => {
      let vacant = pos.board.vacant();

      // consider the this piece to be vacant for continuity purposes
      let board: Bitboard = vacant | squ;

      let diag1_mask = bitboard!(
        00000001
        00000010
        00000100
        00001000
        00010000
        00100000
        01000000
        10000000
      ).shift_vert(squ.rank() as i8 - squ.file() as i8);
      let diag2_mask = bitboard!(
        10000000
        01000000
        00100000
        00010000
        00001000
        00000100
        00000010
        00000001
      ).shift_vert(squ.file() as i8 + squ.rank() as i8 - 7);

      let diag1 = board & diag1_mask | !diag1_mask;
      let diag2 = board & diag2_mask | !diag2_mask;

      // since the range is inclusive on both ends, it implicitly counts the square the piece resides on as vacant
      let ne = diag1.leading_occupied_board(squ) & diag1_mask;
      let sw = diag1.trailing_occupied_board(squ) & diag1_mask;
      let nw = diag2.leading_occupied_board(squ) & diag2_mask;
      let se = diag2.trailing_occupied_board(squ) & diag2_mask;

      ne | sw | nw | se
    },

    Rook => {
      let vacant = pos.board.vacant();

      // consider the this piece to be vacant for continuity purposes
      let board: Bitboard = vacant | squ;

      // no need to mask out the rank because vacant_streak already operates on a rank-first basis
      let horz = board & squ.rank();
      let vert = board & squ.file() | !Bitboard::from(squ.file());

      // since the range is inclusive on both ends, it implicitly counts the square the piece resides on as vacant
      let east = horz.leading_occupied_board(squ);
      let west = horz.trailing_occupied_board(squ);
      let north = vert.leading_occupied_board(squ);
      let south = vert.trailing_occupied_board(squ);

      east | west | (Bitboard::from(squ.file()) & (north | south))
    },

    // the set of all valid moves for the rook and bishop
    Queen => {
      // note: since all other checks have been done (self-capture, king pin), return so they are not repeated
      return self::pseudo_plys(ColoredPiece::new(color, Piece::Rook), squ, pos)
        | self::pseudo_plys(ColoredPiece::new(color, Piece::Bishop), squ, pos)
    },

    King => base_plys(cp, squ),  // the king has no specific non-contexual movement restrictions
  };

  // prevent capturing own pieces
  let plys = plys & !pos.board.occupied_by(color);

  plys
}


#[cfg(test)]
mod test {
  use crate::{bitboard, board::*, position::*};

  // #[test]
  // fn pawn_move() {
  //   let pos = Position {
  //     board, turn: White, castling: CastleState::BOTH, en_passant_target: None, plys_since_progress: 0
  //   };
  //   assert_eq!(, );
  // }
}
