//! Primitive chess data structures, including piece, square, board, and ply
//! representations.

use crate::bitboard;
use crate::util::CollectArray;

pub use self::{Color::*, Piece::*, Square::*};

use std::fmt::{self, Display};
use std::hint::unreachable_unchecked;
use std::num::NonZeroU8;
use std::ops::{
  Add, BitAnd, BitAndAssign, BitOr, BitOrAssign, BitXor,BitXorAssign, Mul, Neg, Not, Sub
};
use std::slice;

use bitflags::bitflags;
use enum_map::{Enum, EnumMap};
use opimps;
use shrinkwraprs::Shrinkwrap;


#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Enum)]
pub enum Color {
  White,
  Black,
}

impl From<bool> for Color {
  fn from(num: bool) -> Self {
    if num { Self::Black } else { Self::White }
  }
}

#[opimps::impl_uni_ops(Not)]
fn not(self: Color) -> Color {
  match self {
    Color::White => Color::Black,
    Color::Black => Color::White,
  }
}


#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Enum)]
pub enum Piece {
  Pawn,
  Knight,
  Bishop,
  Rook,
  Queen,
  King,
}

impl Piece {
  pub const unsafe fn from_unchecked(num: u8) -> Self {
    match num {
      0 => Self::Pawn,
      1 => Self::Knight,
      2 => Self::Bishop,
      3 => Self::Rook,
      4 => Self::Queen,
      5 => Self::King,
      _ => unreachable_unchecked(),
    }
  }
}

impl TryFrom<u8> for Piece {
  type Error = ();

  fn try_from(num: u8) -> Result<Self, Self::Error> {
    Ok(match num {
      0 => Self::Pawn,
      1 => Self::Knight,
      2 => Self::Bishop,
      3 => Self::Rook,
      4 => Self::Queen,
      5 => Self::King,
      _ => Err(())?,
    })
  }
}


/// A packed representation of Color and Piece represented as a single byte.
/// This struct supports null-pointer optimization.
///
/// # Examples
/// ```
/// use passant::board::{Color::*, Piece::*, ColoredPiece};
///
/// assert_eq!(ColoredPiece::new(White, King).get(), (White, King));
/// assert_eq!(ColoredPiece::new(Black, Knight).get(), (Black, Knight));
/// assert_eq!(ColoredPiece::new(White, Pawn).get(), (White, Pawn));
/// assert_eq!(Some(ColoredPiece::new(White, Pawn).get()).unwrap(), (White, Pawn));
/// ```
#[derive(Debug, Clone, Copy, PartialEq, Hash, Eq)]
pub struct ColoredPiece(NonZeroU8);

impl ColoredPiece {
  pub const fn new(color: Color, piece: Piece) -> Self {
    Self(
      // safety: by oring with 0b1000_0000, it's impossible for 0 to be constructed
      unsafe { NonZeroU8::new_unchecked(0b1000_0000 | (piece as u8) | (color as u8) << 3) }
    )
  }

  pub fn get(self) -> (Color, Piece) { (self.color(), self.piece()) }

  pub fn piece(self) -> Piece {
    // safety: self.0 is not mutable and thus is only set from the enum
    // discriminant of Piece and Color. Since the maximum value of Piece is 5,
    // the discriminant takes up no more than three bits, and the values 6 & 7
    // are not legal.
    unsafe { Piece::from_unchecked(self.0.get() & 0b0000_0111) }
  }

  pub fn color(self) -> Color { Color::from(self.0.get() & 0b0000_1000 == 0b0000_1000) }
}


#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Enum)]
pub enum CastleSide {
  King,
  Queen,
}


// The bitflags macro does not pass through block macros on items, so we apply
// the macro on a transparent module instead.
pub use castle::CastleState;

#[allow(non_upper_case_globals)]
mod castle {
  use super::*;

  bitflags! {
    pub struct CastleState: u8 {
      const KingSide = 0b01;
      const QueenSide = 0b10;

      const BOTH = Self::KingSide.bits | Self::QueenSide.bits;
      const NONE = 0;
    }
  }
}


#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Ply {
  Move { from: (ColoredPiece, Square), to: Square, captured: Option<ColoredPiece> },
  Promote { from: (ColoredPiece, Square), to: (ColoredPiece, Square), captured: Option<ColoredPiece> },
  Castle { side: CastleSide },
}


#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Default, Shrinkwrap)]
pub struct Centipawn(pub i16);

impl Centipawn {
  pub fn new(side: Color, score: u16) -> Self {
    match side {
      White => Self(score as i16),
      Black => -Self(score as i16),
    }
  }

  pub fn get(&self) -> (Color, u16) {
    if !self.0.is_negative() {
      (White, self.0 as u16)
    } else {
      (Black, -self.0 as u16)
    }
  }

  pub fn saturating_add(&self, other: Self) -> Self {
    Self(self.0.saturating_add(other.0))
  }
}

// todo: consider TryFrom
impl From<i16> for Centipawn {
  fn from(num: i16) -> Self {
    Self(num * 100)
  }
}

impl From<Centipawn> for i16 {
  fn from(centipawn: Centipawn) -> Self {
    centipawn.0 / 100
  }
}

impl From<f32> for Centipawn {
  fn from(num: f32) -> Self {
    Self((num * 100.0) as i16)
  }
}

impl From<Centipawn> for f32 {
  fn from(centipawn: Centipawn) -> Self {
    centipawn.0 as f32 / 100.0
  }
}

impl Display for Centipawn {
  fn fmt(&self, format: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(format, "{}", self.0 as f32 / 100.0)
  }
}

#[opimps::impl_ops(Add)]
#[allow(unused_braces)]
fn add(self: Centipawn, rhs: Centipawn) -> Centipawn { Centipawn(self.0 + rhs.0) }

#[opimps::impl_ops(Sub)]
#[allow(unused_braces)]
fn sub(self: Centipawn, rhs: Centipawn) -> Centipawn { Centipawn(self.0 - rhs.0) }

#[opimps::impl_ops(Mul)]
#[allow(unused_braces)]
fn mul(self: Centipawn, rhs: Centipawn) -> Centipawn { Centipawn(self.0 * rhs.0) }

#[opimps::impl_uni_ops(Neg)]
#[allow(unused_braces)]
fn neg(self: Centipawn) -> Centipawn { Centipawn(-self.0) }


#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Score {
  /// Evaluation score; color is winning
  Score(Centipawn),
  /// Color wins in this many plys; if 0, Color has won the game by mate
  MateIn(Color, u8),
}

impl Default for Score {
  fn default() -> Self { Self::Score(Centipawn::default()) }
}


/// Squares are mapped using little-endian rank-file mapping, wherein A1 is the
/// least significant bit, H8 is the most significant, and ordering is rank-wise.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Enum)]
#[repr(u8)]
#[rustfmt::skip_fmt]
pub enum Square {
  // A1 = 0, B1 = 1, ... H8 = 63
  A1, B1, C1, D1, E1, F1, G1, H1,
  A2, B2, C2, D2, E2, F2, G2, H2,
  A3, B3, C3, D3, E3, F3, G3, H3,
  A4, B4, C4, D4, E4, F4, G4, H4,
  A5, B5, C5, D5, E5, F5, G5, H5,
  A6, B6, C6, D6, E6, F6, G6, H6,
  A7, B7, C7, D7, E7, F7, G7, H7,
  A8, B8, C8, D8, E8, F8, G8, H8,
}

impl Square {
  pub const EAST: i8 = 1;
  pub const NORTH: i8 = 1;
  pub const SOUTH: i8 = -1;
  pub const WEST: i8 = -1;

  pub const MIN: Square = Self::A1;
  pub const MAX: Square = Self::H8;

  pub fn new(num: u8) -> Self { num.try_into().expect("num must be less than 64") }

  pub fn from_coords(file: File, rank: Rank) -> Self {
    (u8::from(rank) * 8 + u8::from(file)).try_into().unwrap()
  }

  pub fn from_algebraic(coords: &str) -> Option<Self> {
    if coords.len() == 2 && coords.is_ascii() {
      let mut iter = coords.chars();
      let file = match iter.next()? {
        'a' | 'A' => File::A,
        'b' | 'B' => File::B,
        'c' | 'C' => File::C,
        'd' | 'D' => File::D,
        'e' | 'E' => File::E,
        'f' | 'F' => File::F,
        'g' | 'G' => File::G,
        'h' | 'H' => File::H,
        _ => return None,
      };

      let rank = match iter.next()? {
        '1' => Rank::First,
        '2' => Rank::Second,
        '3' => Rank::Third,
        '4' => Rank::Fourth,
        '5' => Rank::Fifth,
        '6' => Rank::Sixth,
        '7' => Rank::Second,
        '8' => Rank::Eight,
        _ => return None,
      };

      Some(Self::from_coords(file, rank))
    } else {
      None
    }
  }

  pub fn into_algebraic(&self) -> String {
    format!(
      "{}{}",
      match self.file() {
        File::A => 'a',
        File::B => 'b',
        File::C => 'c',
        File::D => 'd',
        File::E => 'e',
        File::F => 'f',
        File::G => 'g',
        File::H => 'h',
      },
      u8::from(self.rank()) + 1
    )
  }

  pub fn file(&self) -> File { (u8::from(self) % 8).try_into().unwrap() }

  pub fn rank(&self) -> Rank { (u8::from(self) / 8).try_into().unwrap() }
}

impl TryFrom<u8> for Square {
  type Error = ();

  fn try_from(value: u8) -> Result<Self, Self::Error> {
    match value {
      0 => Ok(Self::A1),
      1 => Ok(Self::B1),
      2 => Ok(Self::C1),
      3 => Ok(Self::D1),
      4 => Ok(Self::E1),
      5 => Ok(Self::F1),
      6 => Ok(Self::G1),
      7 => Ok(Self::H1),
      8 => Ok(Self::A2),
      9 => Ok(Self::B2),
      10 => Ok(Self::C2),
      11 => Ok(Self::D2),
      12 => Ok(Self::E2),
      13 => Ok(Self::F2),
      14 => Ok(Self::G2),
      15 => Ok(Self::H2),
      16 => Ok(Self::A3),
      17 => Ok(Self::B3),
      18 => Ok(Self::C3),
      19 => Ok(Self::D3),
      20 => Ok(Self::E3),
      21 => Ok(Self::F3),
      22 => Ok(Self::G3),
      23 => Ok(Self::H3),
      24 => Ok(Self::A4),
      25 => Ok(Self::B4),
      26 => Ok(Self::C4),
      27 => Ok(Self::D4),
      28 => Ok(Self::E4),
      29 => Ok(Self::F4),
      30 => Ok(Self::G4),
      31 => Ok(Self::H4),
      32 => Ok(Self::A5),
      33 => Ok(Self::B5),
      34 => Ok(Self::C5),
      35 => Ok(Self::D5),
      36 => Ok(Self::E5),
      37 => Ok(Self::F5),
      38 => Ok(Self::G5),
      39 => Ok(Self::H5),
      40 => Ok(Self::A6),
      41 => Ok(Self::B6),
      42 => Ok(Self::C6),
      43 => Ok(Self::D6),
      44 => Ok(Self::E6),
      45 => Ok(Self::F6),
      46 => Ok(Self::G6),
      47 => Ok(Self::H6),
      48 => Ok(Self::A7),
      49 => Ok(Self::B7),
      50 => Ok(Self::C7),
      51 => Ok(Self::D7),
      52 => Ok(Self::E7),
      53 => Ok(Self::F7),
      54 => Ok(Self::G7),
      55 => Ok(Self::H7),
      56 => Ok(Self::A8),
      57 => Ok(Self::B8),
      58 => Ok(Self::C8),
      59 => Ok(Self::D8),
      60 => Ok(Self::E8),
      61 => Ok(Self::F8),
      62 => Ok(Self::G8),
      63 => Ok(Self::H8),
      _ => Err(()),
    }
  }
}

impl From<&Square> for u8 {
  fn from(square: &Square) -> Self {
    *square as u8
  }
}

impl From<Square> for u8 {
  fn from(square: Square) -> Self {
    square as u8
  }
}

impl TryFrom<Bitboard> for Square {
  type Error = ();

  fn try_from(board: Bitboard) -> Result<Self, Self::Error> {
    if board.is_power_of_two() {
      // if the board is a power of two, then it follows that only one bit is set.
      // therefore, the number of trailing zeros indicates the right-ward
      // position of the set bit
      // safety: there can only be up to 64 trailing zeros in a u64
      Ok(Self::new(board.trailing_zeros() as u8))
    } else {
      Err(()) // fails if there is no or multiple bits set
    }
  }
}


#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Enum)]
#[repr(u8)]
pub enum File {
  A, B, C, D, E, F, G, H
}

impl File {
  pub fn new(file: u8) -> Self { file.try_into().expect("invalid file") }
}

impl TryFrom<u8> for File {
  type Error = ();

  fn try_from(file: u8) -> Result<Self, Self::Error> {
    match file {
      0 => Ok(Self::A),
      1 => Ok(Self::B),
      2 => Ok(Self::C),
      3 => Ok(Self::D),
      4 => Ok(Self::E),
      5 => Ok(Self::F),
      6 => Ok(Self::G),
      7 => Ok(Self::H),
      _ => Err(()),
    }
  }
}

impl TryFrom<char> for File {
  type Error = ();

  fn try_from(file: char) -> Result<Self, Self::Error> {
    match file {
      'a' | 'A' => Ok(Self::A),
      'b' | 'B' => Ok(Self::B),
      'c' | 'C' => Ok(Self::C),
      'd' | 'D' => Ok(Self::D),
      'e' | 'E' => Ok(Self::E),
      'f' | 'F' => Ok(Self::F),
      'g' | 'G' => Ok(Self::G),
      'h' | 'H' => Ok(Self::H),
      _ => Err(()),
    }
  }
}

impl From<&File> for u8 {
  fn from(file: &File) -> Self {
    *file as u8
  }
}

impl From<File> for u8 {
  fn from(file: File) -> Self {
    file as u8
  }
}


#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Enum)]
#[repr(u8)]
pub enum Rank {
  First, Second, Third, Fourth, Fifth, Sixth, Seventh, Eight
}

impl Rank {
  pub fn new(rank: u8) -> Self { rank.try_into().expect("invalid rank") }

  pub fn home_rank(color: Color) -> Self {
    match color { White => Self::First, Black => Self::Eight }
  }

  pub fn pawn_rank(color: Color) -> Self {
    match color { White => Self::Second, Black => Self::Seventh }
  }
}

impl TryFrom<u8> for Rank {
  type Error = ();

  fn try_from(rank: u8) -> Result<Self, Self::Error> {
    match rank {
      0 => Ok(Self::First),
      1 => Ok(Self::Second),
      2 => Ok(Self::Third),
      3 => Ok(Self::Fourth),
      4 => Ok(Self::Fifth),
      5 => Ok(Self::Sixth),
      6 => Ok(Self::Seventh),
      7 => Ok(Self::Eight),
      _ => Err(()),
    }
  }
}

impl From<&Rank> for u8 {
  fn from(rank: &Rank) -> Self {
    *rank as u8
  }
}

impl From<Rank> for u8 {
  fn from(rank: Rank) -> Self {
    rank as u8
  }
}


/// A fixed-size set of squares on a chessboard, where each set bit represents
/// the presence of a square in the set.
///
/// This bitboard represents the a1 square with the least significant bit,
/// row by row, from left to right, down to up, up to the h8 square, the most
/// significant bit.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Default, Hash, Shrinkwrap)]
pub struct Bitboard(pub u64);

impl Bitboard {
  pub const NONE: Bitboard = Bitboard(u64::MIN);
  pub const ALL: Bitboard = Bitboard(u64::MAX);

  pub fn new(square: Square) -> Self { Self(1 << u8::from(square)) }

  pub fn from_rank(rank: Rank) -> Self { rank.into() }

  pub fn from_file(file: File) -> Self { file.into() }

  pub fn squares(&self) -> impl Iterator<Item = Square> {
    BitboardSquares { board: self.clone(), offset: 0 }
  }

  pub fn exists(&self) -> bool { self.0 > 0 }

  /// Counts the number of occupied squares on the board.
  pub fn count_occupied(&self) -> u8 { self.0.count_ones() as u8 }

  /// Counts the number of vacant squares on the board.
  pub fn count_vacant(&self) -> u8 { self.0.count_zeros() as u8 }

  /// Counts the number of consecutive occupied squares inclusively starting
  /// from a given square towards the most significant square.
  pub fn leading_occupied(&self, squ: Square) -> u8 { (!self).leading_vacant(squ) }

  /// Counts the number of consecutive occupied squares inclusively starting
  /// from a given square towards the least significant square.
  pub fn trailing_occupied(&self, squ: Square) -> u8 { (!self).trailing_vacant(squ) }

  /// Counts the number of consecutive vacant squares inclusively starting from
  /// a given square towards the most significant square.
  pub fn leading_vacant(&self, squ: Square) -> u8 {
    let squ = u8::from(squ);

    // clamping prevents an overflow when counting into the msb boundary
    ((self.0 >> squ).trailing_zeros() as u8).clamp(0, u8::from(Square::MAX) - squ + 1)
  }

  /// Counts the number of consecutive vacant squares inclusively starting from
  /// a given square towards the least significant square.
  pub fn trailing_vacant(&self, squ: Square) -> u8 {
    let squ = u8::from(squ);

    // clamping prevents an overflow when counting into the lsb boundary
    ((self.0 << (u8::from(Square::MAX) - squ)).leading_zeros() as u8).clamp(0, squ + 1)
  }

  pub fn leading_occupied_board(&self, squ: Square) -> Self {
    let bits = 2_u64.wrapping_pow(self.leading_occupied(squ).into()).wrapping_sub(1);
    Bitboard(bits << u8::from(squ))
  }

  pub fn trailing_occupied_board(&self, squ: Square) -> Self {
    let occupied = self.trailing_occupied(squ);
    let bits = 2_u64.wrapping_pow(occupied.into()).wrapping_sub(1);
    Bitboard(bits << (u8::from(squ).wrapping_sub(occupied).wrapping_add(1)))
  }

  pub fn leading_vacant_board(&self, squ: Square) -> Self {
    let bits = 2_u64.wrapping_pow(self.leading_vacant(squ).into()).wrapping_sub(1);
    Bitboard(bits << u8::from(squ))
  }

  pub fn trailing_vacant_board(&self, squ: Square) -> Self {
    let vacant = self.trailing_vacant(squ);
    let bits = 2_u64.wrapping_pow(vacant.into()).wrapping_sub(1);
    Bitboard(bits << (u8::from(squ).wrapping_sub(vacant).wrapping_add(1)))
  }

  pub fn reverse_files(&self) -> Self {
    let res = self.0.to_le_bytes()
      .iter()
      .map(|byte| byte.reverse_bits())
      .collect_array::<8>()
      .unwrap();

    Self(u64::from_le_bytes(res))
  }

  pub fn reverse_ranks(&self) -> Self {
    let res = self.0.to_le_bytes()
      .iter()
      .rev()
      .cloned()
      .collect_array::<8>()
      .unwrap();

    Self(u64::from_le_bytes(res))
  }

  /// Logically shifts the bitboard vertically and horizontally.
  /// See `Square::NORTH`, `Square::EAST`, `Square::SOUTH`, `Square::WEST` for
  /// constants used in shifting.
  pub fn shift(&self, num: i8) -> Self {
    Self(match num.signum() {
      1 | 0 => self.checked_shl(num.abs() as u32).unwrap_or(0),
      -1 => self.checked_shr(num.abs() as u32).unwrap_or(0),
      _ => unreachable!(),
    })
  }

  /// Logically shifts the bitboard vertically.
  /// See `Square::NORTH`, `Square::SOUTH` for constants used in shifting.
  pub fn shift_vert(&self, vert: i8) -> Self {
    Self(
      if vert >= 0 {
        self.checked_shl(8 * vert.abs() as u32).unwrap_or(0)
      } else {
        self.checked_shr(8 * vert.abs() as u32).unwrap_or(0)
      }
    )
  }

  /// Logically shifts the bitboard horizontally.
  /// See `Square::EAST`, `Square::WEST` for constants used in shifting.
  pub fn shift_horz(&self, horz: i8) -> Self {
    // a mask for which bits remain after performing a left shift operation
    const MASK: [u64; 8] = [
      0b11111111_11111111_11111111_11111111_11111111_11111111_11111111_11111111,
      0b11111110_11111110_11111110_11111110_11111110_11111110_11111110_11111110,
      0b11111100_11111100_11111100_11111100_11111100_11111100_11111100_11111100,
      0b11111000_11111000_11111000_11111000_11111000_11111000_11111000_11111000,
      0b11110000_11110000_11110000_11110000_11110000_11110000_11110000_11110000,
      0b11100000_11100000_11100000_11100000_11100000_11100000_11100000_11100000,
      0b11000000_11000000_11000000_11000000_11000000_11000000_11000000_11000000,
      0b10000000_10000000_10000000_10000000_10000000_10000000_10000000_10000000,
    ];

    if horz.abs() >= 8 { return Self(0); }

    Self(
      if horz >= 0 {
        // shift the board but mask out the bits that would of had zeros shifted in
        self.0.checked_shl(horz.abs() as u32).unwrap_or(0)
          & MASK[horz as usize]
      } else {
        // reversing the bits flips the mask into one appropriate for a right shift
        self.0.checked_shr(horz.abs() as u32).unwrap_or(0)
          & MASK[horz.abs() as usize].reverse_bits()
      }
    )
  }
}

impl From<&Square> for Bitboard {
  fn from(square: &Square) -> Self { Self(1 << u8::from(square)) }
}

impl From<Square> for Bitboard {
  fn from(square: Square) -> Self { Self::from(&square) }
}

impl From<&Bitboard> for u64 {
  fn from(board: &Bitboard) -> Self { board.0 }
}

impl From<Bitboard> for u64 {
  fn from(board: Bitboard) -> Self { board.0 }
}

impl From<&Rank> for Bitboard {
  fn from(rank: &Rank) -> Self {
    bitboard!(
      00000000
      00000000
      00000000
      00000000
      00000000
      00000000
      00000000
      11111111
    ).shift_vert(u8::from(rank) as i8 * Square::NORTH)
  }
}

impl From<Rank> for Bitboard {
  fn from(rank: Rank) -> Self { Bitboard::from(&rank) }
}

impl From<&File> for Bitboard {
  fn from(file: &File) -> Self {
    bitboard!(
      10000000
      10000000
      10000000
      10000000
      10000000
      10000000
      10000000
      10000000
    ).shift_horz(u8::from(file) as i8 * Square::EAST)
  }
}

impl From<File> for Bitboard {
  fn from(file: File) -> Self {
    Bitboard::from(&file)
  }
}

impl Display for Bitboard {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    let res = self
      .to_be_bytes()
      .iter()
      .map(|byte| format!("{:08b}", byte.reverse_bits()))
      .fold(String::new(), |acc, str| format!("{}{}\n", acc, str));

    write!(f, "{}", res.trim_end())
  }
}

#[opimps::impl_uni_ops(Not)]
#[allow(unused_braces)]
fn not(self: Bitboard) -> Bitboard { Bitboard(!self.0) }

#[opimps::impl_ops(BitOr)]
#[allow(unused_braces)]
fn bitor(self: Bitboard, rhs: Bitboard) -> Bitboard { Bitboard(self.0 | rhs.0) }

#[opimps::impl_ops(BitXor)]
#[allow(unused_braces)]
fn bitxor(self: Bitboard, rhs: Bitboard) -> Bitboard { Bitboard(self.0 ^ rhs.0) }

#[opimps::impl_ops(BitAnd)]
#[allow(unused_braces)]
fn bitand(self: Bitboard, rhs: Bitboard) -> Bitboard { Bitboard(self.0 & rhs.0) }

#[opimps::impl_ops_assign(BitOrAssign)]
#[allow(unused_braces)]
fn bitor_assign(self: Bitboard, rhs: Bitboard) { self.0 |= rhs.0 }

#[opimps::impl_ops_assign(BitXorAssign)]
#[allow(unused_braces)]
fn bitxor_assign(self: Bitboard, rhs: Bitboard) { self.0 ^= rhs.0 }

#[opimps::impl_ops_assign(BitAndAssign)]
#[allow(unused_braces)]
fn bitand_assign(self: Bitboard, rhs: Bitboard) { self.0 &= rhs.0 }

macro_rules! impl_bitboard_ops {
  ($id:ident: $type:ty => $expr:expr) => {
    #[opimps::impl_ops(BitOr)]
    #[allow(unused_braces)]
    fn bitor(self: Bitboard, $id: $type) -> Bitboard { self | $expr }

    #[opimps::impl_ops(BitXor)]
    #[allow(unused_braces)]
    fn bitxor(self: Bitboard, $id: $type) -> Bitboard { self ^ $expr }

    #[opimps::impl_ops(BitAnd)]
    #[allow(unused_braces)]
    fn bitand(self: Bitboard, $id: $type) -> Bitboard { self & $expr }

    #[opimps::impl_ops_assign(BitOrAssign)]
    #[allow(unused_braces)]
    fn bitor_assign(self: Bitboard, $id: $type) { self.bitor_assign($expr) }

    #[opimps::impl_ops_assign(BitXorAssign)]
    #[allow(unused_braces)]
    fn bitxor_assign(self: Bitboard, $id: $type) { self.bitxor_assign($expr) }

    #[opimps::impl_ops_assign(BitAndAssign)]
    #[allow(unused_braces)]
    fn bitand_assign(self: Bitboard, $id: $type) { self.bitand_assign($expr) }
  };
}

impl_bitboard_ops!(rhs: Square => Bitboard::from(rhs));
impl_bitboard_ops!(rhs: File => Bitboard::from(rhs));
impl_bitboard_ops!(rhs: Rank => Bitboard::from(rhs));


#[macro_export]
/// Macro to construct a bitboard from a bitstring literal.
///
/// # Examples
/// ```
/// use passant::bitboard;
///
/// let white_knights = bitboard!(
///   00000000
///   00000000
///   00000000
///   00000000
///   00000000
///   00000000
///   00000000
///   01000010
/// //abcdefgh
/// );
macro_rules! bitboard {
  ( $($rank:literal)+ ) => {
    $crate::board::Bitboard(u64::from_be_bytes([
      $(
        #[allow(clippy::zero_prefixed_literal)]
        u8::from_str_radix(&($rank as u32).to_string(), 2)
          .expect("binary parsing failed")
          .reverse_bits(),
      )+
    ]))
  }
}


#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct BitboardSquares {
  board: Bitboard,
  offset: u8,
}

impl Iterator for BitboardSquares {
  type Item = Square;

  fn next(&mut self) -> Option<Self::Item> {
    let bit_pos = self.board.0.trailing_zeros() as u8;
    if bit_pos < 64 {
      self.board.0 >>= bit_pos + 1;
      self.offset += bit_pos + 1;
      Some(Square::new(self.offset - 1))
    } else {
      None
    }
  }
}


/// A mapping of bitboards for each piece type guaranteed to never have multiple
/// pieces on the same square.
///
/// Since this structure uses bitboards, it is significantly quicker to compute
/// checks involving move generation & material calculation, at a slight cost to
/// memory usage, and requires callers to uphold it's safety guarantee.
///
/// # Safety
/// This structure must never have multiple pieces present on the same square,
/// e.g. there cannot be the same bit set on two or more bitboards.
/// This property needs to be upheld by code mutating the structure.
#[derive(Debug, Clone, PartialEq, Eq, Hash, Default, Shrinkwrap)]
pub struct Board(EnumMap<Color, EnumMap<Piece, Bitboard>>);

impl Board {
  // todo: a fallible constructor

  pub fn pairs(&self) -> impl Iterator<Item = (Color, Piece, Bitboard)> {
    self.0.into_iter()
      .flat_map(|(color, pieces)|
        pieces.into_iter().map(move |(piece, board)| (color, piece, board))
      )
  }

  pub unsafe fn pairs_mut(&mut self) -> impl Iterator<Item = (Color, Piece, &mut Bitboard)> {
    self.0.iter_mut()
      .flat_map(|(color, pieces)|
        pieces.iter_mut().map(move |(piece, board)| (color, piece, board))
      )
  }

  pub fn boards(&self) -> impl Iterator<Item = Bitboard> {
    self.pairs().map(|(.., board)| board)
  }

  pub fn squares(&self) -> impl Iterator<Item = (ColoredPiece, Square)> {
    self.0.into_iter()
      .flat_map(|(color, pieces)|
        pieces.into_iter().flat_map(
          move |(piece, board)| board.squares().map(move |square| (ColoredPiece::new(color, piece), square))
        )
      )
  }

  pub fn pieces(&self) -> &EnumMap<Color, EnumMap<Piece, Bitboard>> { &self.0 }

  pub unsafe fn pieces_mut(&mut self) -> &mut EnumMap<Color, EnumMap<Piece, Bitboard>> { &mut self.0 }

  pub fn occupied(&self) -> Bitboard {
    self.boards().fold(Default::default(), BitOr::bitor)
  }

  pub fn occupied_by(&self, color: Color) -> Bitboard {
    self.pieces()[color].values().fold(Default::default(), BitOr::bitor)
  }

  pub fn vacant(&self) -> Bitboard { !self.occupied() }
}


impl From<AltBoard> for Board {
  fn from(board: AltBoard) -> Self { Self::from(&board) }
}

impl From<&AltBoard> for Board {
  fn from(board: &AltBoard) -> Self {
    Self(board.pieces().iter().fold(Default::default(), |mut board, piece| {
      if let (square, Some(piece)) = piece {
        board[piece.color()][piece.piece()] |= square;
      }
      board
    }))
  }
}


/// An alternative "fat" board representation using arrays of optional colored
/// pieces, mapped by squares.
///
/// Since colored pieces support null pointer optimization, this data structure
/// only uses 64 bytes. This has a slightly smaller memory footprint than
/// [Board] does, however performing most checks necessary for move/material
/// generation is expensive and not done in hardware, and conversion between
/// boards is also expensive.
#[derive(Debug, Clone, PartialEq, Eq, Hash, Default)]
pub struct AltBoard(pub EnumMap<Square, Option<ColoredPiece>>);

impl AltBoard {
  pub fn new(board: [Option<ColoredPiece>; 64]) -> Self {
    AltBoard(EnumMap::from_array(board))
  }

  pub fn pieces(&self) -> &EnumMap<Square, Option<ColoredPiece>> { &self.0 }

  pub fn ranks(&self) -> slice::Chunks<'_, Option<ColoredPiece>> {
    self.0.as_slice().chunks(8)
  }

  pub fn to_ascii(&self) -> String {
    self.ranks().rev().enumerate().fold(String::new(), |out, (i, rank)| {
      let mut out = rank.iter().fold(out, |mut out, square| {
        out += match square.map(|cp| cp.get()) {
          Some((Color::White, Piece::Pawn)) => "P",
          Some((Color::White, Piece::Knight)) => "N",
          Some((Color::White, Piece::Bishop)) => "B",
          Some((Color::White, Piece::Rook)) => "R",
          Some((Color::White, Piece::Queen)) => "Q",
          Some((Color::White, Piece::King)) => "K",
          Some((Color::Black, Piece::Pawn)) => "p",
          Some((Color::Black, Piece::Knight)) => "n",
          Some((Color::Black, Piece::Bishop)) => "b",
          Some((Color::Black, Piece::Rook)) => "r",
          Some((Color::Black, Piece::Queen)) => "q",
          Some((Color::Black, Piece::King)) => "k",
          None => ".",
        };
        out
      });

      if i != 7 { out += "\n"; }
      out
    })
  }

  pub fn to_ansi(&self) -> String {
    self.ranks().rev().enumerate().fold(String::new(), |out, (i, rank)| {
      let mut out = rank.iter().fold(out, |mut out, square| {
        out += match &square.map(|cp| cp.get()) {
          Some((Color::White, _)) => "\x1b[39m",  // reset color
          Some((Color::Black, _)) => "\x1b[31m",  // set red
          None => "\x1b[90m",  // set gray
        };

        out += match square.map(|cp| cp.get()) {
          Some((_, Piece::Pawn)) => "p",
          Some((_, Piece::Knight)) => "n",
          Some((_, Piece::Bishop)) => "b",
          Some((_, Piece::Rook)) => "r",
          Some((_, Piece::Queen)) => "q",
          Some((_, Piece::King)) => "k",
          None => ".",
        };
        out
      });

      // reset color at the end to avoid mucking up subsequent strings
      out += if i != 7 { "\n" } else { "\x1b[39m" };
      out
    })
  }

  pub fn to_unicode(&self) -> String {
    self.ranks().rev().enumerate().fold(String::new(), |out, (i, rank)| {
      let mut out = rank.iter().fold(out, |mut out, square| {
        out += match square.map(|cp| cp.get()) {
          Some((Color::White, Piece::Pawn)) => "\u{2659}",
          Some((Color::White, Piece::Knight)) => "\u{2658}",
          Some((Color::White, Piece::Bishop)) => "\u{2657}",
          Some((Color::White, Piece::Rook)) => "\u{2656}",
          Some((Color::White, Piece::Queen)) => "\u{2655}",
          Some((Color::White, Piece::King)) => "\u{2654}",
          Some((Color::Black, Piece::Pawn)) => "\u{265F}",
          Some((Color::Black, Piece::Knight)) => "\u{265E}",
          Some((Color::Black, Piece::Bishop)) => "\u{265D}",
          Some((Color::Black, Piece::Rook)) => "\u{265C}",
          Some((Color::Black, Piece::Queen)) => "\u{265B}",
          Some((Color::Black, Piece::King)) => "\u{265A}",
          // None => "\u{2003}", // em-space for alignment
          None => ".", // em-space for alignment
        };
        out
      });

      if i != 7 { out += "\n"; }
      out
    })
  }
}

impl From<Board> for AltBoard {
  fn from(board: Board) -> Self { Self::from(&board) }
}

impl From<&Board> for AltBoard {
  fn from(board: &Board) -> Self {
    Self(
      board.pairs()
        .map(|(color, piece, board)| (ColoredPiece::new(color, piece), board))
        .fold(EnumMap::default(), |mut arr, (piece, board)| {
          for square in board.squares() {
            arr[square] = Some(piece);
          }
          arr
        }),
    )
  }
}


#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn square_coords() {
    let sq = Square::from_coords(File::G, Rank::Third);
    assert_eq!(sq, Square::G3);
    assert_eq!(u8::from(sq), 8*2 + 6);
    assert_eq!(sq.file(), File::G);
    assert_eq!(sq.rank(), Rank::Third);
  }

  #[test]
  fn bitboard_fmt() {
    let board = bitboard! {
      00000011
      00000000
      00000000
      00000000
      00000000
      00000000
      10000000
      11100000
    };
    assert_eq!(
      "00000011\n00000000\n00000000\n00000000\n00000000\n00000000\n10000000\n11100000",
      format!("{}", board)
    );
  }

  #[test]
  fn bitboard_squares() {
    let board = bitboard! {
      00001100
      00000000
      00000000
      00000000
      00000000
      00000000
      10000000
      11100000
    };
    assert_eq!(
      board.squares().collect::<Vec<_>>(),
      vec![A1, B1, C1, A2, E8, F8]
    )
  }

  #[test]
  fn bitboard_rev_rank() {
    assert_eq!(
      bitboard!(
        00000000
        00000000
        00010000
        00110000
        00001100
        00001100
        00000000
        00000000
      )
      .reverse_ranks(),
      bitboard!(
        00000000
        00000000
        00001100
        00001100
        00110000
        00010000
        00000000
        00000000
      )
    )
  }

  #[test]
  fn bitboard_rev_file() {
    assert_eq!(
      bitboard!(
        00000000
        00000000
        00010000
        00110000
        00001100
        00001100
        00000000
        00000000
      )
      .reverse_files(),
      bitboard!(
        00000000
        00000000
        00001000
        00001100
        00110000
        00110000
        00000000
        00000000
      )
    )
  }

  #[test]
  fn bitboard_shift() {
    let shifted = bitboard!(
      00000000
      00000000
      00010000
      00110000
      00001100
      00001100
      00000000
      00000000
    )
    .shift_vert(2 * Square::NORTH).shift_horz(3 * Square::WEST);

    assert_eq!(
      shifted,
      bitboard!(
        10000000
        10000000
        01100000
        01100000
        00000000
        00000000
        00000000
        00000000
      )
    )
  }

  #[test]
  fn bitboard_leading() {
    let board = bitboard!(
      00000011
      00000000
      00010000
      00110000
      00001100
      00011100
      00000000
      00000000
    //abcdefgh
    );

    assert_eq!(board.leading_occupied(D3), 3);
    assert_eq!(board.leading_occupied(E3), 2);
    assert_eq!(board.leading_occupied(F3), 1);
    assert_eq!(board.leading_vacant(F1), 3 + 8 + 3);
    assert_eq!(board.leading_occupied(F8), 0);
    assert_eq!(board.leading_occupied(G8), 2);
    assert_eq!(board.leading_occupied(H8), 1);

    assert_eq!(
      board.leading_occupied_board(D3),
      bitboard!(
        00000000
        00000000
        00000000
        00000000
        00000000
        00011100
        00000000
        00000000
      )
    );
    assert_eq!(
      board.leading_occupied_board(G8),
      bitboard!(
        00000011
        00000000
        00000000
        00000000
        00000000
        00000000
        00000000
        00000000
      )
    );
  }

  #[test]
  fn bitboard_trailing() {
    let board = bitboard!(
      00000011
      00000000
      00010000
      00110000
      00001100
      00011100
      00000000
      00000000
    //abcdefgh
    );

    assert_eq!(board.trailing_occupied(F3), 3);
    assert_eq!(board.trailing_vacant(A1), 1);
    assert_eq!(board.trailing_vacant(D1), 4);

    assert_eq!(
      board.trailing_occupied_board(F3),
      bitboard!(
        00000000
        00000000
        00000000
        00000000
        00000000
        00011100
        00000000
        00000000
      )
    );
    assert_eq!(
      board.trailing_vacant_board(D1),
      bitboard!(
        00000000
        00000000
        00000000
        00000000
        00000000
        00000000
        00000000
        11110000
      )
    );
  }
}
